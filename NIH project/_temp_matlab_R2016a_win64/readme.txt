 
   Table of Contents
   -----------------

   1. R2016a Product List

   2. System Requirements

   3. Installation

   4. Help

   5. Contact Information


1. R2016a Product and Platform List
   --------------------------------

   MATLAB 9.0 - glnxa64 maci64 win64
   Simulink 8.7 - glnxa64 maci64 win64
   Aerospace Blockset  3.17 - glnxa64 maci64 win64
   Aerospace Toolbox  2.17 - glnxa64 maci64 win64
   Antenna Toolbox  2.0 - glnxa64 maci64 win64
   Audio System Toolbox  1.0 - glnxa64 maci64 win64
   Bioinformatics Toolbox  4.6 - glnxa64 maci64 win64
   Communications System Toolbox  6.2 - glnxa64 maci64 win64
   Computer Vision System Toolbox  7.1 - glnxa64 maci64 win64
   Control System Toolbox  10.0 - glnxa64 maci64 win64
   Curve Fitting Toolbox  3.5.3 - glnxa64 maci64 win64
   DO Qualification Kit  3.1 - glnxa64 maci64 win64
   DSP System Toolbox  9.2 - glnxa64 maci64 win64
   Data Acquisition Toolbox  3.9 - win64
   Database Toolbox  6.1 - glnxa64 maci64 win64
   Datafeed Toolbox  5.3 - glnxa64 maci64 win64
   Econometrics Toolbox  3.4 - glnxa64 maci64 win64
   Embedded Coder  6.10 - glnxa64 maci64 win64
   Filter Design HDL Coder  3.0 - glnxa64 maci64 win64
   Financial Instruments Toolbox  2.3 - glnxa64 maci64 win64
   Financial Toolbox  5.7 - glnxa64 maci64 win64
   Fixed-Point Designer  5.2 - glnxa64 maci64 win64
   Fuzzy Logic Toolbox  2.2.23 - glnxa64 maci64 win64
   Global Optimization Toolbox  3.4 - glnxa64 maci64 win64
   HDL Coder  3.8 - glnxa64 maci64 win64
   HDL Verifier  5.0 - glnxa64 win64
   IEC Certification Kit  3.7 - glnxa64 maci64 win64
   Image Acquisition Toolbox  5.0 - glnxa64 maci64 win64
   Image Processing Toolbox  9.4 - glnxa64 maci64 win64
   Instrument Control Toolbox  3.9 - glnxa64 maci64 win64
   LTE System Toolbox  2.2 - glnxa64 maci64 win64
   MATLAB Coder  3.1 - glnxa64 maci64 win64
   MATLAB Compiler  6.2 - glnxa64 maci64 win64
   MATLAB Compiler SDK  6.2 - glnxa64 maci64 win64
   MATLAB Distributed Computing Server  6.8 - glnxa64 maci64 win64
   MATLAB Production Server  2.3 - glnxa64 maci64 win64
   MATLAB Report Generator  5.0 - glnxa64 maci64 win64
   Mapping Toolbox  4.3 - glnxa64 maci64 win64
   Model Predictive Control Toolbox  5.2 - glnxa64 maci64 win64
   Model-Based Calibration Toolbox  5.1 - win64
   Neural Network Toolbox  9.0 - glnxa64 maci64 win64
   OPC Toolbox  4.0.1 - win64
   Optimization Toolbox  7.4 - glnxa64 maci64 win64
   Parallel Computing Toolbox  6.8 - glnxa64 maci64 win64
   Partial Differential Equation Toolbox  2.2 - glnxa64 maci64 win64
   Phased Array System Toolbox  3.2 - glnxa64 maci64 win64
   Polyspace Bug Finder  2.1 - glnxa64 maci64 win64
   Polyspace Code Prover  9.5 - glnxa64 maci64 win64
   RF Toolbox  3.0 - glnxa64 maci64 win64
   Robotics System Toolbox  1.2 - glnxa64 maci64 win64
   Robust Control Toolbox  6.1 - glnxa64 maci64 win64
   Signal Processing Toolbox  7.2 - glnxa64 maci64 win64
   SimBiology  5.4 - glnxa64 maci64 win64
   SimEvents  5.0 - glnxa64 maci64 win64
   SimRF  5.0 - glnxa64 maci64 win64
   Simscape  4.0 - glnxa64 maci64 win64
   Simscape Driveline  2.10 - glnxa64 maci64 win64
   Simscape Electronics  2.9 - glnxa64 maci64 win64
   Simscape Fluids  2.0 - glnxa64 maci64 win64
   Simscape Multibody  4.8 - glnxa64 maci64 win64
   Simscape Power Systems  6.5 - glnxa64 maci64 win64
   Simulink 3D Animation  7.5 - glnxa64 maci64 win64
   Simulink Code Inspector  2.5 - glnxa64 win64
   Simulink Coder  8.10 - glnxa64 maci64 win64
   Simulink Control Design  4.3 - glnxa64 maci64 win64
   Simulink Design Optimization  3.0 - glnxa64 maci64 win64
   Simulink Design Verifier  3.1 - glnxa64 maci64 win64
   Simulink Desktop Real-Time  5.2 - maci64 win64
   Simulink PLC Coder  2.1 - win64
   Simulink Real-Time  6.4 - win64
   Simulink Report Generator  5.0 - glnxa64 maci64 win64
   Simulink Test  2.0 - glnxa64 maci64 win64
   Simulink Verification and Validation  3.11 - glnxa64 maci64 win64
   Spreadsheet Link  3.2.5 - win64
   Stateflow  8.7 - glnxa64 maci64 win64
   Statistics and Machine Learning Toolbox  10.2 - glnxa64 maci64 win64
   Symbolic Math Toolbox  7.0 - glnxa64 maci64 win64
   System Identification Toolbox  9.4 - glnxa64 maci64 win64
   Trading Toolbox  3.0 - win64
   Vehicle Network Toolbox  3.1 - win64
   Vision HDL Toolbox  1.2 - glnxa64 win64
   WLAN System Toolbox  1.1 - glnxa64 maci64 win64
   Wavelet Toolbox  4.16 - glnxa64 maci64 win64


2. System Requirements
   -------------------

   For information about the system requirements for this 
   release, see the installation guide for your system. A copy
   in PDF format is available in the top-level directory 
   of the installation disk. 

   For the most up-to-date system requirements information,
   go to the Support page on the MathWorks Web site, www.mathworks.com.


3. Installation
   ------------

   3.1 Installation Commands

       On UNIX systems:  install
        
       On PCs:  setup.exe

       On Mac:  InstallForMacOSX
      
   3.2 Installation Instructions

       For more information about the installation procedure,
       see the installation guide for your system. A copy
       in PDF format is available in the top-level directory 
       of the installation disk. 


4. Getting Help
   ------------

    MATLAB includes an extensive online help system. Use the
    Help menu in the MATLAB menu bar to access Help.

    You can also view the documentation and search the technical 
    support solutions database at the MathWorks Web site at
 
        http://www.mathworks.com/support/

    to find answers to hundreds of commonly asked questions.


5. Contact Information
   -------------------
 
    For contact information visit

    http://www.mathworks.com/company/aboutus/contact_us/


MATLAB is a Registered Trademark of The MathWorks, Inc.
UNIX is a Registered Trademark of the Open Group Company
------------------------------------------------------------------------
Copyright 2001-2016 The MathWorks, Inc.
 
------------------------------------------------------------------------

