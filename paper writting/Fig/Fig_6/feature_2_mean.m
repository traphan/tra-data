clear all
close all

%set(groot,'defaultFigureColor', [0.8 0.8 0.8])
set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('D:\Texas Tech\EMBC paper\Fig\Fig_6\data')
addpath('D:\Texas Tech\EMBC paper\Fig\Fig_6\ARMA_MODEL')

data=load('004.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
N=2;
y=Highpass(y,Fs,0.5);

[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);
X=[D1,D2,A2];

X=[D1,D2,A2];
for i=1:size(X,2)
p(i)=AR_oder(X(:,i));
AR=ar_model(X(:,i),p(i));
Y(:,i) = AR_MODELING( X(:,i),AR,p(i));
end

e=Y-X;

T=150;
w=30;
S=floor((length(y)-T)/w)+1;
theta=[];

%m=47; % subject#1
%m=53; % subject#2
%m=65; % subject#3
m=65; % subject#4

for i=1:S
theta_temp =MA(D1(1+w*(i-1):1+w*(i-1)+T),e(:,1),m);
 theta=[theta,theta_temp];
end

delta=[];
for j=1:S-3
    delta_temp=((theta(1,j+3)-theta(1,j+2))^2+(theta(1,j+2)-theta(1,j+1))^2+(theta(1,j+1)-theta(1,j))^2)/3;
    delta=[delta,delta_temp];
end

delta(length(delta)+1:length(delta)+4)=delta(end);

for i=1:round(length(delta)/7)
    m(i)=mean(delta(7*(i-1)+1:7*i));
     for j=7*(i-1)+1:7*i
       M(j)=m(i);
   end
end


figure(1)
plot([0:1:length(y)-1]*Ts,y,'-');
xlabel('time (sec)','FontSize', 12)
ylabel('Amplitude','FontSize', 12)
xlim([0 160])
%set(gca,'FontSize',12);
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

figure(2)
plot([0:1:length(delta)-1],delta)
xlabel('Time (s)','FontSize', 12)
ylabel('Successsive Difference of the first MA coefficients','FontSize', 12)

figure(3)
plot([0:1:length(M)-1],M)
xlabel('Time (s)','FontSize', 12)
ylabel('Mean of SDFs','FontSize', 12)

figure(4)
plot([0:1:length(M)-1],M)
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end
xlabel('Time (s)','FontSize', 12)
ylabel('Mean of SDFs','FontSize', 12)


%for subject 1
%z(1:49)=0;
%z(50:84)=1;
%z(85:161)=0;

%for subject 2
%z(1:42)=0;
%z(43:77)=1;
%z(78:112)=0;
%z(120:161)=0;

%for subject 3
%z(1:35)=0;
%z(36:70)=1;
%z(71:161)=0;

%for subject 4
z(1:35)=0;
z(36:77)=1;
z(78:126)=0;
z(127:133)=1;
z(134:161)=0;


X=[M',z'];

