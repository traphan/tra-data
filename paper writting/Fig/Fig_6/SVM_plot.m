
clear all
close all

%set(groot,'defaultFigureColor', [0.8 0.8 0.8])
set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('D:\Texas Tech\EMBC paper\Fig\Fig_6\data')

data=load('004.mat');

data = xlsread('feature.xlsx','sub4_mean');

X=data(:,1:2);
Y=data(:,3);
Y1=Y;
Y1=cell(length(Y),1); 
for i=1:length(Y)
if Y(i)==1
    Y1{i}='corrupted';
else
    Y1{i}='clean';
end
end

svmStruct = svmtrain(X,Y1,'showplot',true);
C = svmclassify(svmStruct,X);
xlabel('Mean of parameter 1', 'FontSize', 11)
ylabel('Mean of parameter 2', 'FontSize', 11)

