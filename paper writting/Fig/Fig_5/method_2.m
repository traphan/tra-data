clear all
close all
set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\TraMap\OneDrive - Texas Tech University\Research\EMBC paper\EMBC paper\paper writting\Fig\Fig_4\data')
addpath('C:\Users\TraMap\OneDrive - Texas Tech University\Research\EMBC paper\EMBC paper\paper writting\Fig\Fig_4\ARMA_MODEL')


data=load('003.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
N=2;
y=Highpass(y,Fs,0.5);

[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);

X=[D1,D2,A2];
for i=1:size(X,2)
p(i)=AR_oder(X(:,i));
AR=ar_model(X(:,i),p(i));
Y(:,i) = AR_MODELING( X(:,i),AR,p(i));
end

e=Y-X;

T=150;
w=30;
S=floor((length(y)-T)/w)+1;
theta=[];

%m=47; % subject#1
%m=53; % subject#2
m=65; % subject#3
%m=65; % subject#4

for i=1:S
theta_temp =MA(D1(1+w*(i-1):1+w*(i-1)+T),e(:,1),m);
 theta=[theta,theta_temp];
end


delta=[];
for j=1:S-3
    delta_temp=((theta(1,j+3)-theta(1,j+2))^2+(theta(1,j+2)-theta(1,j+1))^2+(theta(1,j+1)-theta(1,j))^2)/3;
    delta=[delta,delta_temp];
end
delta(length(delta)+1:length(delta)+4)=delta(end);

%thresh=27.1331; %subject#1
%thresh= 11.3187; %subject#2
thresh=13.0622; %subject#3
%thresh= 10.8305; %subject#4

s=length(delta)
for i=1:s
    if delta(i)> thresh %subject#2
        MNA(i)=1;
    else
       MNA(i)=0;
    end
end

subplot(3,1,1)
plot([0:1:length(y)-1]*Ts,y,'-');
ylabel('PPG','FontSize', 12)
xlim([0 160])
ylim([-25 25])

for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

subplot(3,1,2)
plot([0:1:length(delta)-1],delta)
ylabel('Parameter 2', 'FontSize', 12)
ylim([min(delta)-5 max(delta)+5])
for i=1:round(length(MNA)/7-1)
line([i*7 i*7], ylim,'Color','k');
end

subplot(3,1,3)
plot([0:1:length(MNA)-1],MNA)
xlabel('Time (s)', 'FontSize', 12)
ylabel('Decision', 'FontSize', 12)
ylim([-0.05 1.05])
for i=1:round(length(MNA)/7-1)
line([i*7 i*7], ylim,'Color','k');
end


figure

subplot(3,1,1)
plot([0:1:length(y)-1]*Ts,y,'-');
ylabel('PPG','FontSize', 12)
xlim([0 160])
ylim([-25 25])

subplot(3,1,2)
plot([0:1:length(delta)-1],delta)
ylabel('Parameter 2', 'FontSize', 12)
ylim([min(delta)-5 max(delta)+5])
xlim([0 160])

subplot(3,1,3)
plot([0:1:length(MNA)-1],MNA)
xlabel('Time (s)', 'FontSize', 12)
ylabel('Decision', 'FontSize', 12)
ylim([-0.05 1.05])
xlim([0 160])



