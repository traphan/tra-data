function [ y ] = LLH( X, AR )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
p = length(AR)-1;
T = length(X);
A=AR(2:p+1);
sigma = 1;
y = 0;
e = zeros(T,1);
 for ii = p+1:T
        e(ii) = X(ii) + A*X(ii-1:-1:ii-p); 
         y = y + e(ii)^2;
    end
y = y/(2*sigma^2);
y = y + log(sigma^2)*(T-p)/2 + log(2*pi)*(T-p)/2;
end

