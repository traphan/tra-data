clear all
close all

set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\TraMap\OneDrive - Texas Tech University\Research\EMBC paper\EMBC paper\paper writting\Fig\Fig_4\data')
addpath('C:\Users\TraMap\OneDrive - Texas Tech University\Research\EMBC paper\EMBC paper\paper writting\Fig\Fig_4\ARMA_MODEL')

data=load('002.mat');
y1=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y1); 
N=2;
y=Highpass(y1,Fs,0.5);

[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);
X=[D1,D2,A2];

T=150;
Z=30;
S=floor((length(y)-T)/Z)+1;

for i=1:S
p(i)=AR_oder(X(1+Z*(i-1):1+Z*(i-1)+T,1));
end

p(i+1)=p(end);

%thresh=6;  %subject #1
thresh=6;  %subject #2
%thresh=26;  %subject #3
%thresh=21;  %subject #4

for i=1:S
    if p(i)> thresh
        MNA(i)=1;
    else
        MNA(i)=0;
    end
end


subplot(3,1,1)
plot([0:1:length(y)-1]*Ts,y,'-');
ylabel('PPG','FontSize', 12)
xlim([0 160])
ylim([min(y)-5 max(y)+5])

for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

subplot(3,1,2)
plot([0:1:length(p)-1],p)
ylabel('AR order', 'FontSize', 12)
xlim([0 160])
ylim([min(p)-5 max(p)+5])
for i=1:round(length(p)/7-1)
line([i*7 i*7], ylim,'Color','k');
end

subplot(3,1,3)
plot([0:1:length(MNA)-1],MNA)
xlabel('Time (s)', 'FontSize', 12)
ylabel('Decision', 'FontSize', 12)
ylim([-0.05 1.05])
for i=1:round(length(MNA)/7-1)
line([i*7 i*7], ylim,'Color','k');
end

figure

subplot(3,1,1)
plot([0:1:length(y)-1]*Ts,y,'-');
ylabel('PPG','FontSize', 12)
xlim([0 160])
ylim([min(y)-5 max(y)+5])


subplot(3,1,2)
plot([0:1:length(p)-1],p)
ylabel('Parameter 1', 'FontSize', 12)
xlim([0 160])
ylim([min(p)-5 max(p)+5])
                
subplot(3,1,3)
plot([0:1:length(MNA)-1],MNA)
xlabel('Time (s)', 'FontSize', 12)
ylabel('Decision', 'FontSize', 12)
ylim([-0.05 1.05])
xlim([0 160])