LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

#opencv
OPENCVROOT:= C:\Users\traphan\OpenCV-android-sdk
OPENCV_CAMERA_MODULES:=off
OPENCV_INSTALL_MODULES:=on
OPENCV_LIB_TYPE:=SHARED
include ${OPENCVROOT}/sdk/native/jni/OpenCV.mk

LOCAL_MODULE := imageProcessing
LOCAL_SRC_FILES := main.cpp testfunction.cpp heartRateCalculator.cpp spo2calculation.cpp matrix.cpp
#FILE_LIST := $(wildcard $(LOCAL_PATH)/*.cpp)
#LOCAL_SRC_FILES += $(FILE_LIST:$(LOCAL_PATH)/%=%)
LOCAL_C_INCLUDES += $(LOCAL_PATH)
LOCAL_LDLIBS += -llog
#LOCAL_CFLAGS += -std=c++11

include $(BUILD_SHARED_LIBRARY)