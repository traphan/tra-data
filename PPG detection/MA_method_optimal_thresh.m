clear all
close all
set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\data')
addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\ARMA_MODEL')

data=load('001.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
N=2;

%bandpass filter
%----------------------------------------------------------------
Wn=[0.5 4];
[b,a] = butter(6,Wn/(Fs/2),'bandpass');
yn_filt=filtfilt(b,a,y);
y=Highpass(yn_filt,Fs,0.5);

%y=Highpass(y,Fs,0.5);



[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);
X=[D1,D2,A2];

T=150;
w=30;
S=floor((length(y)-T)/w);
A=[];

m=20; % subject#1
%m=53; % subject#2
%m=65; % subject#3
%m=65; % subject#4

for i=1:S
p=AR_oder(D1(1+w*(i-1):1+w*(i-1)+T));  
AR=ar_model(D1(1+w*(i-1):1+w*(i-1)+T),p);
Y= AR_MODELING(D1(1+w*(i-1):1+w*(i-1)+T),AR,p);
e=Y-D1(1+w*(i-1):1+w*(i-1)+T);
q(i) =MA_order(D1(1+w*(i-1):1+w*(i-1)+T),e);
end

q(length(q)+1)=q(end);
q(length(q)+2)=q(end);
a=min(q);
b=max(q);

TPR=[];
FPR=[];
TH_matrix=[];

%for subject 1
C(1:7)=0;
C(8:12)=1;
C(13:23)=0;

%for subject 2
%C(1:6)=0;
%C(7:11)=1;
%C(12:16)=0;
%C(17)=1;
%C(18:23)=0;

%for subject 3
%C(1:5)=0;
%C(6:10)=1;
%C(11:23)=0;

%for subject 4
%C(1:5)=0;
%C(6:11)=1;
%C(12:18)=0;
%C(19)=1;
%C(20:23)=0;

for i=a:0.001:b
    TH_matrix=[TH_matrix i];
    MNA_temp=q>i;
    for j=1:round(length(MNA_temp)/7)
        if sum(MNA_temp(1+7*(j-1):7*j))>0
            MNA(j)=1;
        else
            MNA(j)=0;
        end
    end
    
    TP=0;

for k=1:length(MNA)
    if MNA(k)==C(k)&&C(k)==1
        TP=TP+1;
    end
end

TN=0;

for k=1:length(MNA)
    if MNA(k)==C(k)&&C(k)==0
        TN=TN+1;
    end
end

FP=0;

for k=1:length(MNA)
    if C(k)==0 &&MNA(k)==1
        FP=FP+1;
    end
end

FN=0;

for k=1:length(MNA)
    if C(k)==1 &&MNA(k)==0
        FN=FN+1;
    end
end

TPR_temp= TP/(TP+FN);
FPR_temp=FP/(FP+TN);

TPR=[TPR TPR_temp];
FPR=[FPR FPR_temp];
    
end

Y=TPR-FPR;
[optimal TH_index]=max(Y);
thresh=TH_matrix(TH_index)



