
clear all
close all

%set(groot,'defaultFigureColor', [0.8 0.8 0.8])
set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\Result_Simulation\data')
data = xlsread('feature.xlsx','fea_mean');
data_SNR = xlsread('feature.xlsx','fea_mean_SNR15');
X=data(:,1:2);
Y=data(:,3);
X_SNR=data_SNR(:,1:2);

cvFolds = crossvalind('Kfold', Y, 10);
cp = classperf(Y);


Y1=Y;
Y1=cell(length(Y),1); 
for i=1:length(Y)
if Y(i)==1
    Y1{i}='corrupted';
else
    Y1{i}='clean';
end
end

svmStruct = svmtrain(X,Y1,'boxconstraint',10,'showplot',true);
Group = svmclassify(svmStruct,X_SNR,'showplot',true);
xlabel('Mean of parameter 1', 'FontSize', 11)
ylabel('Mean of parameter 2', 'FontSize', 11)

figure
for i = 1:10                                 %# for each fold
    testIdx = (cvFolds == i);                %# get indices of test instances
    trainIdx = ~testIdx;                     %# get indices training instances
     svmModel = svmtrain(X(trainIdx,:), Y(trainIdx));
      pred = svmclassify(svmModel, X(testIdx,:));
     cp = classperf(cp, pred, testIdx);
end

C=cp.CorrectRate;

alpha=svmStruct.Alpha;
SV=svmStruct.SupportVectors;
coefs=sum(alpha'*SV);



