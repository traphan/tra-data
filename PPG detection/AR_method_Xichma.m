clear all
close all

set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\data')
addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\ARMA_MODEL')

data=load('004.mat');
y1=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y1); 
N=2;

%bandpass filter
%----------------------------------------------------------------
Wn=[0.5 4];
[b,a] = butter(6,Wn/(Fs/2),'bandpass');
yn_filt=filtfilt(b,a,y1);
y=Highpass(yn_filt,Fs,0.5);


%DWT
%----------------------------------------------------------------
[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);
X=[D1,D2,A2];


%optimal AR order
%----------------------------------------------------------------
T=150;
Z=30;
S=floor((length(y)-T)/Z)+1;
SIGMA=[];
for i=1:S
[p(i),sigma]=AR_oder_Xichma(X(1+Z*(i-1):1+Z*(i-1)+T,1));
SIGMA=[SIGMA;sigma];
end
p(i+1)=p(end);


%mean of optimal AR order
%----------------------------------------------------------------
for i=1:round(length(p)/7)
    m(i)=mean(p(7*(i-1)+1:7*i));
     for j=7*(i-1)+1:7*i
       M(j)=m(i);
   end
end


%segment labeling
%----------------------------------------------------------------
%for subject 1
z(1:49)=0;
z(50:84)=1;
z(85:161)=0;

%for subject 2
%z(1:42)=0;
%z(43:77)=1;
%z(78:112)=0;
%z(113:119)=1;
%z(120:161)=0;

%for subject 3
%z(1:35)=0;
%z(36:70)=1;
%z(71:161)=0;

%for subject 4
%z(1:35)=0;
%z(36:77)=1;
%z(78:126)=0;
%z(127:133)=1;
%z(134:161)=0;

X=[M',z'];


%plot result
%----------------------------------------------------------------
figure
subplot(411)
plot([0:1:length(y)-1]*Ts,y1,'-');
xlabel('time (sec)','FontSize', 12)
ylabel('Original PPG','FontSize', 12)
xlim([0 160])
%set(gca,'FontSize',12);
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

subplot(412)
plot([0:1:length(y)-1]*Ts,y,'-');
xlabel('time (sec)','FontSize', 12)
ylabel('Filtered PPG','FontSize', 12)
xlim([0 160])
%set(gca,'FontSize',12);
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

subplot(413)
plot([0:1:length(p)-1],p)
xlabel('Time (s)','FontSize', 12)
ylabel('optimal AR orders','FontSize', 12)
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

subplot(414)
plot([0:1:length(M)-1],M)
xlabel('Time (s)','FontSize', 12)
ylabel('Mean of optimal AR orders','FontSize', 12)
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end


