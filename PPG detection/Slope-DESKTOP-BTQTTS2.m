clear all
close all

set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);


data=load('004.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
y=Highpass(y,Fs,0.5);
y=y*(-1);

T=210;
w=30;
S=floor((length(y)-T)/w)+1;

for i=1:S
 y1=y(1+w*(i-1):1+w*(i-1)+T,1);
for j=1:length(y1)-1
    slope(j)=y1(j+1)-y1(j);
end
A=slope(slope>0);
B=slope(slope<0);
P(i)=max(A);
N(i)=min(B);
R(i)=abs(P(i)/N(i)); 
end
R(i+1)=R(end);
R(i+2)=R(end);
R(i+3)=R(end);

subplot(211)
plot([0:1:length(y)-1]*Ts,y,'-');
xlabel('time (sec)','FontSize', 12)
ylabel('PPG','FontSize', 12)
xlim([0 160])
%set(gca,'FontSize',12);
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

subplot(212)
plot(R)
xlabel('Time (s)','FontSize', 12)
ylabel('slope ratio','FontSize', 12)
xlim([0 160])
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end


