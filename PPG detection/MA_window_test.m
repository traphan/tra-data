clear all
close all

set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\data')
addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\ARMA_MODEL')

data=load('003.mat');
y1=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y1); 
N=2;


%bandpass filter
%------------------------------------------------------------------
%Wn=[0.5 4];
%[b,a] = butter(6,Wn/(Fs/2),'bandpass');
%yn_filt=filtfilt(b,a,y1);
%y=Highpass(yn_filt,Fs,0.5);
y=Highpass(y1,Fs,0.5);

%DWT
%----------------------------------------------------------------
[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);
X=[D1,D2,A2];


%obtain optimal MA order
%----------------------------------------------------------------

%T=210;
w=30;

for k=4:7
    T=k*30;
    S=floor((length(y)-T)/w)+1;
for i=1:S
p=AR_oder(D1(1+w*(i-1):1+w*(i-1)+T));  
AR=ar_model(D1(1+w*(i-1):1+w*(i-1)+T),p);
Y= AR_MODELING(D1(1+w*(i-1):1+w*(i-1)+T),AR,p);
e=D1(1+w*(i-1):1+w*(i-1)+T)-Y;
q(i) =MA_order(D1(1+w*(i-1):1+w*(i-1)+T),e);
end
subplot(4,1,k-3)
plot(q)
for t=1:round(length(y)*Ts/7-1)
line([t*7 t*7], ylim,'Color','k');
end
title(sprintf('window length t= %d s', k))
end
