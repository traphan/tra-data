function [ order ] = AR_oder( X)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
n=150;
LOGL = zeros(1,n); %Initialize
PQ = zeros(1,n);
for p = 1:n
        mod = ar_model(X,p);
      logL = LLH(X,mod);
        LOGL(p) = -logL;
        PQ(p) = p;
     
end
%LOGL = reshape(LOGL,n*n,1);
%PQ = reshape(PQ,n*n,1);
[aic1,bic1] = aicbic(LOGL,PQ,length(X));
[a b]=min(aic1);
[c d]=min(bic1);
order= b;
end

