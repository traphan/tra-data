function [ order ] = MA_order( y, e )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
n=150;
LOGL = zeros(1,n); %Initialize
PQ = zeros(1,n);
for m = 1:n
        theta = MA(y,e,m);
      logL = LLH_MA(y,e,theta,m);
        LOGL(m) = -logL;
        PQ(m) = m;
     
end
%LOGL = reshape(LOGL,n*n,1);
%PQ = reshape(PQ,n*n,1);
[aic1,bic1] = aicbic(LOGL,PQ,length(y));
[a b]=min(aic1);
[c d]=min(bic1);
order= b;

end

