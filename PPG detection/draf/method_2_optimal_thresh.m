clear all
close all
set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\TTU\Research\EMBC paper\code_v2\data')
addpath('C:\TTU\Research\EMBC paper\code_v2\ARMA_MODEL')

data=load('004.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
N=2;
y=Highpass(y,Fs,0.5);

[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);

X=[D1,D2,A2];
for i=1:size(X,2)
p(i)=AR_oder(X(:,i));
AR=ar_model(X(:,i),p(i));
Y(:,i) = AR_MODELING( X(:,i),AR,p(i));
end

e=Y-X;

T=150;
w=30;
S=floor((length(y)-T)/w)+1;
theta=[];

%m=47; % subject#1
%m=53; % subject#2
%m=65; % subject#3
m=65; % subject#4

for i=1:S
theta_temp =MA(D1(1+w*(i-1):1+w*(i-1)+T),e(:,1),m);
 theta=[theta,theta_temp];
end


delta=[];
for j=1:S-3
    delta_temp=((theta(1,j+3)-theta(1,j+2))^2+(theta(1,j+2)-theta(1,j+1))^2+(theta(1,j+1)-theta(1,j))^2)/3;
    delta=[delta,delta_temp];
end

delta(length(delta)+1:length(delta)+4)=delta(end);
a=min(delta);
b=max(delta);

TPR=[];
FPR=[];
TH_matrix=[];

%for subject 1
%C(1:7)=0;
%C(8:12)=1;
%C(13:23)=0;
%step=0.001;

%for subject 2
%C(1:6)=0;
%C(7:11)=1;
%C(12:16)=0;
%C(17)=1;
%C(18:23)=0;
%step=0.001;

%for subject 3
%C(1:5)=0;
%C(6:10)=1;
%C(11:23)=0;
%step=0.1;

%for subject 4
C(1:5)=0;
C(6:11)=1;
C(12:18)=0;
C(19)=1;
C(20:23)=0;
step=0.01;

for i=a:step:b
    TH_matrix=[TH_matrix i];
    MNA_temp=delta>i;
    for j=1:round(length(MNA_temp)/7)
        if sum(MNA_temp(1+7*(j-1):7*j))>0
            MNA(j)=1;
        else
            MNA(j)=0;
        end
    end
    
    TP=0;

for k=1:length(MNA)
    if MNA(k)==C(k)&&C(k)==1
        TP=TP+1;
    end
end

TN=0;

for k=1:length(MNA)
    if MNA(k)==C(k)&&C(k)==0
        TN=TN+1;
    end
end

FP=0;

for k=1:length(MNA)
    if C(k)==0 &&MNA(k)==1
        FP=FP+1;
    end
end

FN=0;

for k=1:length(MNA)
    if C(k)==1 &&MNA(k)==0
        FN=FN+1;
    end
end

TPR_temp= TP/(TP+FN);
FPR_temp=FP/(FP+TN);

TPR=[TPR TPR_temp];
FPR=[FPR FPR_temp];
    
end

Y=TPR-FPR;
[optimal TH_index]=max(Y);
thresh=TH_matrix(TH_index)



