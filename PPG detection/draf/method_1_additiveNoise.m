clear all
close all

set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\data')
addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\ARMA_MODEL')

data=load('001.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
N=2;
y=y(2500:4810);
%y=y(1:1500);
%highpass filter
%-------------------------------------------------
y1=Highpass(y,Fs,0.5);

%add additive noise
%-------------------------------------------------
SNR_dB=0;
SNR=10^(SNR_dB/10);
yn=y1(400:600);
sigpowerLin=mean(y1(400:600).^2);
sigpower=10*log10(sigpowerLin);
x1=awgn(y1(400:600),10,sigpower);
x=awgn(y1(501:1500),10,sigpower);
x2=sqrt(sigpowerLin/SNR)*randn(1,length(yn));
y_SNR=[y1(1:500);x;y1(1501:2311)];

%bandpass filter
%-------------------------------------------------
Wn=[0.5 12];
[b,a] = butter(6,Wn/(Fs/2),'bandpass');
%y=Highpass(y,Fs,0.5);
y_filt=filter(b,a,y_SNR);


[C,L] = wavedec(y_filt,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);
X=[D1,D2,A2];

T=150;
Z=30;
S=floor((length(y)-T)/Z)+1;

for i=1:S
p(i)=AR_oder(X(1+Z*(i-1):1+Z*(i-1)+T,1));
end

%p(i+1)=p(end);

a=6;  %subject #1
%a=6;  %subject #2
%a=26;  %subject #3
%a=21;  %subject #4

for i=1:S
    if p(i)> a
        MNA(i)=1;
    else
        MNA(i)=0;
    end
end

figure(1)
plot([0:1:length(y_filt)-1]*Ts,y_filt,'-');
xlabel('Time (sec)','FontSize', 12)
ylabel('PPG','FontSize', 12)
xlim([0 77])
%set(gca,'FontSize',12);
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

figure(2)
plot([0:1:length(MNA)-1],MNA)
xlabel('Time (s)', 'FontSize', 12)
ylabel('MNA detection method 1', 'FontSize', 12)
ylim([0 1.02])
for i=1:round(length(p)/7)
line([i*7 i*7], ylim,'Color','k');
end


figure(3)
plot([0:1:length(p)-1],p)
xlabel('Time (s)', 'FontSize', 12)
ylabel('Optimal AR order', 'FontSize', 12)

for i=1:round(length(p)/7)
line([i*7 i*7], ylim,'Color','k');
end

figure(4)

L=length(y_SNR);
NFFT = 2^nextpow2(L);
f = Fs/2*linspace(0,1,NFFT/2+1);

  spec = fft(y_SNR,NFFT)/L; 
  plot(f,2*abs(spec(1:NFFT/2+1)))
  title('FFT of y SNR') 

