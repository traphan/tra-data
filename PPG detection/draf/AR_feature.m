clear all
close all

set(groot,'defaultFigureColor', [0.8 0.8 0.8])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\TraMap\OneDrive - Texas Tech University\Research\EMBC paper\EMBC paper\code_v2\data')
addpath('C:\Users\TraMap\OneDrive - Texas Tech University\Research\EMBC paper\EMBC paper\code_v2\ARMA_MODEL')

data=load('001.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
N=2;
y=Highpass(y,Fs,0.5);
y=y-mean(y);

figure(1)
plot([1:1:length(y)]*Ts,y,'-');
xlabel('time (sec)','FontSize', 12)
ylabel('Amplitude','FontSize', 12)
xlim([1 160])
%set(gca,'FontSize',12);

[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);
X=[D1,D2,A2];

T=150;
Z=30;
S=floor((length(y)-T)/Z);

for i=1:S
p1(i)=AR_oder(X(1+Z*(i-1):1+Z*(i-1)+T,1));
end

figure(1)
plot([1:1:length(y)]*Ts,y,'-');
xlabel('time (sec)','FontSize', 12)
ylabel('Amplitude','FontSize', 12)
xlim([0 160])
%set(gca,'FontSize',12);
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

%for subject 1
%z(1:48)=0;
%z(49:84)=1;
%z(85:159)=0;

%for subject 2
%z(1:41)=0;
%z(42:77)=1;
%z(78:112)=0;
%z(113:119)=1;
%z(120:159)=0;

%for subject 3
%z(1:34)=0;
%z(35:70)=1;
%z(71:159)=0;

%for subject 4
z(1:34)=0;
z(35:77)=1;
z(78:125)=0;
z(126:133)=1;
z(134:159)=0;


X=[p1',z'];
