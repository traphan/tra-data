clear all
close all

%set(groot,'defaultFigureColor', [0.8 0.8 0.8])
set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\data')
addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\ARMA_MODEL')

data=load('001.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
N=2;

%bandpass filter
%----------------------------------------------------------------
Wn=[0.5 4];
[b,a] = butter(6,Wn/(Fs/2),'bandpass');
yn_filt=filtfilt(b,a,y);
y=Highpass(yn_filt,Fs,0.5);

%y=Highpass(y,Fs,0.5);



[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);
X=[D1,D2,A2];

T=150;
w=30;
S=floor((length(y)-T)/w);
A=[];

m=20; % subject#1
%m=53; % subject#2
%m=65; % subject#3
%m=65; % subject#4

for i=1:S
p=AR_oder(D1(1+w*(i-1):1+w*(i-1)+T));  
AR=ar_model(D1(1+w*(i-1):1+w*(i-1)+T),p);
Y= AR_MODELING(D1(1+w*(i-1):1+w*(i-1)+T),AR,p);
e=Y-D1(1+w*(i-1):1+w*(i-1)+T);
q(i) =MA_order(D1(1+w*(i-1):1+w*(i-1)+T),e);
end

%%thresh=0; %subject1
%thresh=0; %subject2
%thresh=1; %subject2
%thresh=9; %subject3
for i=1:S
    if q(i)> thresh
        MNA(i)=1;
    else
        MNA(i)=0;
    end
end


subplot(3,1,1)
plot([0:1:length(y)-1]*Ts,y,'-');
ylabel('PPG','FontSize', 12)
xlim([0 160])
ylim([min(y)-5 max(y)+5])

for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

subplot(3,1,2)
plot([0:1:length(q)-1],q)
ylabel('Parameter 1', 'FontSize', 12)
xlim([0 160])
ylim([-3 8])
for i=1:round(length(q)/7-1)
line([i*7 i*7], ylim,'Color','k');
end

subplot(3,1,3)
plot([0:1:length(MNA)-1],MNA)
xlabel('Time (s)', 'FontSize', 12)
ylabel('Decision', 'FontSize', 12)
ylim([-0.05 1.05])
for i=1:round(length(MNA)/7-1)
line([i*7 i*7], ylim,'Color','k');
end

figure

subplot(3,1,1)
plot([0:1:length(y)-1]*Ts,y,'-');
ylabel('PPG','FontSize', 12)
xlim([0 160])
ylim([min(y)-5 max(y)+5])


subplot(3,1,2)
plot([0:1:length(q)-1],q)
ylabel('Parameter 2', 'FontSize', 12)
xlim([0 160])
ylim([min(q)-2 max(q)+2])
                
subplot(3,1,3)
plot([0:1:length(MNA)-1],MNA)
xlabel('Time (s)', 'FontSize', 12)
ylabel('Decision', 'FontSize', 12)
ylim([-0.05 1.05])
xlim([0 160])





