clear all
close all
set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\data')
addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\ARMA_MODEL')

data=load('001.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
N=2;
y=Highpass(y,Fs,0.5);
%y1=y;
y=y(2500:4810);
x=awgn(y(501:1500),0,'measured');
y=[y(1:500);x;y(1501:2311)];

[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);

X=[D1,D2,A2];
for i=1:size(X,2)
p(i)=AR_oder(X(:,i));
AR=ar_model(X(:,i),p(i));
Y(:,i) = AR_MODELING( X(:,i),AR,p(i));
end

e=Y-X;

T=150;
w=30;
S=floor((length(y)-T)/w)+1;
theta=[];

m=70;
%m=47; % subject#1
%m=53; % subject#2
%m=65; % subject#3
%m=65; % subject#4

for i=1:S
theta_temp =MA(D1(1+w*(i-1):1+w*(i-1)+T),e(:,1),m);
 theta=[theta,theta_temp];
end


delta=[];
for j=1:S-3
    delta_temp=((theta(1,j+3)-theta(1,j+2))^2+(theta(1,j+2)-theta(1,j+1))^2+(theta(1,j+1)-theta(1,j))^2)/3;
    delta=[delta,delta_temp];
end
delta(length(delta)+1:length(delta)+4)=delta(end);

%thresh=27.1331; %subject#1
%thresh= 11.3187; %subject#2
%thresh=13.0622; %subject#3
thresh= 10.8305; %subject#4

s=length(delta)
for i=1:s
    if delta(i)> thresh %subject#2
        MNA(i)=1;
    else
       MNA(i)=0;
    end
end

figure(1)
plot(theta(1,:));
xlabel('Time (s)', 'FontSize', 12)
ylabel('First MA coefficient', 'FontSize', 12)

figure(2)
plot([0:1:length(delta)-1],delta)
xlabel('Time (sec)', 'FontSize', 12)
ylabel('mean square of successive difference of the first MA coefficients', 'FontSize', 12)
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end


figure(3)
plot([0:1:length(MNA)-1],MNA);
xlabel('Time (s)','FontSize', 12)
ylabel('MNA detection','FontSize', 12)
%xlim([1 160])
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

figure(4)
plot([0:1:length(y)-1]*Ts,y,'-');
xlabel('Time (s)','FontSize', 12)
ylabel('PPG','FontSize', 12)
xlim([1 77])
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end



