clear all
close all

set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

data=load('001.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
y=y(2500:4810);

%highpass filter
%-------------------------------------------------
y_H=Highpass(y,Fs,0.5);


%add noise
%------------------------------------
x=y_H(400:600);
L=length(x);
SNR_dB=0;
SNR=10^(SNR_dB/10);
sigpowerLin=sum(abs(x).^2)/(L);
sigpower=10*log10(sigpowerLin);

%add additive noise: method 1
%-------------------------------------------------
noiseSigma = sqrt(sigpowerLin/SNR);
n1 = noiseSigma*randn(1,L);
yn_meth1=x+n1';
plot(yn_meth1);

%add additive noise: method 2
%-------------------------------------------------
yn_meth2=awgn(x,SNR_dB,sigpower);
hold on
plot(yn_meth2,'r')

legend('calculated','use function');
title('add noise to signal');

%bandpass filter
%-------------------------------------------
%y_SNR=[y1(1:500);x;y1(1501:2311)];
Wn=[0.5 4];
[b,a] = butter(6,Wn/(Fs/2),'bandpass');
yn_filt=filtfilt(b,a,yn_meth1);
figure
plot(yn_filt);

%Frequency domain
%--------------------------------------------------------------
figure
L1=length(yn_meth1);
NFFT = 2^nextpow2(L1);
f = Fs/2*linspace(0,1,NFFT/2+1);
  spec = fft(yn_filt,NFFT)/L1; 
  plot(f,2*abs(spec(1:NFFT/2+1)))
  title('FFT of yn') 
  


