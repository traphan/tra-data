
clear all
close all

%set(groot,'defaultFigureColor', [0.8 0.8 0.8])
set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\pnguy\OneDrive - Texas Tech University\Research\PPG detection\data')
data1 = xlsread('AR_slope.xlsx','sub4_1');
data2 = xlsread('AR_slope.xlsx','sub4_2');


X_test=data1(:,1:2);
X_train=data2(:,1:2);

Y_test=data1(:,3);
Y_train=data2(:,3);

%cvFolds = crossvalind('Kfold', Y, 10);
%cp = classperf(Y);

svmStruct = svmtrain(X_train,Y_train,'boxconstraint',10,'showplot',true);
Group = svmclassify(svmStruct,X_test);
xlabel('AR', 'FontSize', 11)
ylabel('Slope', 'FontSize', 11)

%--------------------------------------------------------------------------------------

data=load('004.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
y=Highpass(y,Fs,0.5);

figure
subplot(211)
plot([0:1:length(y)-1]*Ts,y,'-');
xlabel('time (sec)','FontSize', 12)
ylabel('PPG','FontSize', 12)
xlim([0 160])
%set(gca,'FontSize',12);
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end

subplot(212)
plot(Group)
xlabel('Time (s)','FontSize', 12)
ylabel('MNA decision','FontSize', 12)
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end
