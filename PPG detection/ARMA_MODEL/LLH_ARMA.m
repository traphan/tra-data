function [ y ] = LLH_ARMA( X, theta,p,q )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
l=length(X);
insert=zeros(q,1);
X=[X;insert];
L=p+1;
T = length(X);
sigma = 1;
y = 0;
e = zeros(T,1);
 for ii = L+q:T
        e(ii) = X(ii) +theta(1:p)'*X(ii-1:-1:ii-p)+ theta(p+1:p+q)'*e(ii-1:-1:ii-q); 
         y = y + e(ii)^2;
    end
y = y/(2*sigma^2);
y = y + log(sigma^2)*(l-p)/2 + log(2*pi)*(l-p)/2;
end


