
function [ theta ] = ARMA( y,e,p,q )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
y=y';
e=e';
L=p+q+1;
N=length(y);
z=y(L+1:end);
Z=[];
for j= L:N-1
    K=y(j:-1:j-p+1);
    K=[K,-e(j:-1:j-q+1)];
    Z=[Z;K];
end

theta=-pinv(Z'*Z)*(Z'*z');

end

