function [ order ] = ARMA_order( y, e,p )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
n=100;
LOGL = zeros(1,n); %Initialize
PQ = zeros(1,n);
for q = 1:n
      theta = ARMA(y,e,p,q);
      logL = LLH_ARMA(y,theta,p,q);
        LOGL(q) = -logL;
        PQ(q) = q;
     
end
%LOGL = reshape(LOGL,n*n,1);
%PQ = reshape(PQ,n*n,1);
[aic1,bic1] = aicbic(LOGL,PQ,length(y));
[a b]=min(aic1);
[c d]=min(bic1);
order= b;

end

