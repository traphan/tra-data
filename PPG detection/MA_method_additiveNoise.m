clear all
close all

set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\data')
addpath('C:\Users\traphan\OneDrive - Texas Tech University\Research\PPG detection\ARMA_MODEL')

data=load('001.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
N=2;

y=y(2500:4810);

%add noise
%------------------------------------
y_H=Highpass(y,Fs,0.5);
x=y_H(400:600);
L=length(x);
SNR_dB=-10;
SNR=10^(SNR_dB/10);
sigpowerLin=sum(abs(x).^2)/(L);
sigpower=10*log10(sigpowerLin);
noiseSigma = sqrt(sigpowerLin/SNR);
n = noiseSigma*randn(1,1000);
yn=y;
yn(501:1500)=yn(501:1500)+n';


%bandpass filter
%------------------------------------
Wn=[0.5 4];
[b,a] = butter(6,Wn/(Fs/2),'bandpass');
yn_filt=filtfilt(b,a,yn);
y=Highpass(yn_filt,Fs,0.5);


[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);
X=[D1,D2,A2];

T=150;
w=30;
S=floor((length(y)-T)/w)+1;

for i=1:S
p=AR_oder(D1(1+w*(i-1):1+w*(i-1)+T));  
AR=ar_model(D1(1+w*(i-1):1+w*(i-1)+T),p);
Y= AR_MODELING(D1(1+w*(i-1):1+w*(i-1)+T),AR,p);
e=Y-D1(1+w*(i-1):1+w*(i-1)+T);
q(i) =MA_order(D1(1+w*(i-1):1+w*(i-1)+T),e);
end

q(i+1)=q(end);
q(i+2)=q(end);
q(i+3)=q(end);
q(i+4)=q(end);

for i=1:round(length(q)/7)
    m(i)=mean(q(7*(i-1)+1:7*i));
     for j=7*(i-1)+1:7*i
       M(j)=m(i);
   end
end

z(1:14)=0;
z(15:56)=1;
z(57:77)=0;

X=[M',z'];

figure(1)
plot([0:1:length(y)-1]*Ts,yn_filt,'-');
xlabel('time (sec)','FontSize', 12)
ylabel('Amplitude','FontSize', 12)
xlim([0 77])
%set(gca,'FontSize',12);
for i=1:round(length(y)*Ts/7-1)
line([i*7 i*7], ylim,'Color','k');
end
