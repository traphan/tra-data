clear all; clc; close all;
load('c_result_HR.mat');
load('matlab_result_hr');

Fs = 300;

figure(1);
subplot(2,1,1); 
plot([1:1:length(data2)]/Fs,data2); legend('C')
ylim([0,250]); 
xlabel('\it{t} \rm(sec)');
ylabel('\it{HR} \rm{(au)}');

subplot(2,1,2); set(gca, 'YLim', [0, 400]);
plot([1:1:length(ECGHRref)]/Fs,ECGHRref); legend('Matlab')
ylim([0,250]);
xlabel('\it{t} \rm(sec)');
ylabel('\it{HR} \rm{(au)}');

figure(2);
plot([1:1:length(data2)]/Fs,data2,'k-',[1:1:length(ECGHRref)]/Fs,ECGHRref, 'r--' );
legend('C', 'Matlab')
xlabel('\it{t} \rm(sec)');
ylabel('\it{HR} \rm{(au)}');
set(gca, 'YLim', [0, 220]);