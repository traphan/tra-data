
clear all
close all

% Solve a Pattern Recognition Problem with a Neural Network
% Script generated by Neural Pattern Recognition app
% Created 13-May-2017 15:39:46
%
% This script assumes these variables are defined:
%
%   simpleclassInputs - input data.
%   simpleclassTargets - target data.

%data= xlsread('features_AR_RR.xlsx');
data1 = xlsread('features_AR_RR_insert.xlsx','sub4_1');
data2 = xlsread('features_AR_RR_insert.xlsx','sub4_2');
%training data
X_train=data2(:,1:2)';
Y_train=data2(:,3)';
Y_train=Y_train+1;

%X1=data(1:474,1:2)';
%t1=data(1:474,3)';
%t1=t1+1;

%testing data

X_test=data1(:,1:2)';
Y_test=data1(:,3)';
Y_test=Y_test+1;

%X2=data(475:end,1:2)';
%t2=data(475:end,3)'


% Create a Probability Neuro Network and train
T1 = ind2vec(Y_train)
net = newpnn(X_train,T1,1);
Y1 = sim(net,X_train)
Yc1 = vec2ind(Y1)

%testing the new data
Y2 = sim(net,X_test);
Yc2 = vec2ind(Y2)


% View the Network
view(net)

% Plots
% Uncomment these lines to enable various plots.
%figure, plotperform(tr)
%figure, plottrainstate(tr)
%figure, ploterrhist(e)
%figure, plotconfusion(t,y)
%figure, plotroc(t,y)

subplot(211)
plot(Y_test,'g')
ylim([1;2.1]);
for i=1:round(length(Yc2)/210-1)
line([i*210 i*210], ylim,'Color','k');
end
legend('true label')

subplot(212)
plot(Yc2,'r')
ylim([1;2.1]);
for i=1:round(length(Yc2)/210-1)
line([i*210 i*210], ylim,'Color','k');
end
legend('PNN decision');