clear all
close all

set(0,'defaultFigureColor', [1 1 1])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

addpath('C:\Users\traphan\OneDrive_Backup\OneDrive - Texas Tech University\Research\PNN\PNN demo\ARMA_MODEL')

data=load('004.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
l_s=length(y); 
N=2;
y=Highpass(y,Fs,0.5);

%DWT
%----------------------------------------------------------------
[C,L] = wavedec(y,N,'db4');
[cD1,cD2] = detcoef(C,L,[1,2]);
A2 = wrcoef('a',C,L,'db4',2);
D1 = wrcoef('d',C,L,'db4',1);
D2 = wrcoef('d',C,L,'db4',2);
X=[D1,D2,A2];


%optimal AR order
%----------------------------------------------------------------
T=210;
Z=30;
S=floor((length(y)-T)/Z)+1;

for i=1:S
p(i)=AR_oder(X(1+Z*(i-1):Z*(i-1)+T,1));
p_insert(1+Z*(i-1):Z*(i-1)+Z)=p(i);
end

p_insert=p_insert';

%segment labeling
%----------------------------------------------------------------
%for subject 1
%z(1:1469)=0;
%z(1470:2519)=1;
%z(2520:4924)=0;

%for subject 2
%z(1:1259)=0;
%z(1260:2309)=1;
%z(2310:3359)=0;
%z(3360:3569)=1;
%z(3570:4924)=0;

%for subject 3
%z(1:1049)=0;
%z(1050:2099)=1;
%z(2100:4924)=0;

%for subject 4
z(1:1049)=0;
z(1050:2309)=1;
z(2310:3779)=0;
z(3780:3989)=1;
z(3990:4924)=0;

%X=[M',z'];

z=z';
%plot result
%----------------------------------------------------------------
figure
plot(p)


