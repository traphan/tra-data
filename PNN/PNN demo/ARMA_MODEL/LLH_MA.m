function [ y ] = LLH_MA( X, E, theta,m )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
L=m+1;
T = length(X);
sigma = 1;
y = 0;
e = zeros(T,1);
 for ii = L+1:T
        e(ii) = X(ii) - theta'*E(ii-1:-1:ii-m); 
         y = y + e(ii)^2;
    end
y = y/(2*sigma^2);
y = y + log(sigma^2)*(T-m)/2 + log(2*pi)*(T-m)/2;
end


