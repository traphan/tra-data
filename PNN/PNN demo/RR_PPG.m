clear all
close all

set(groot,'defaultFigureColor', [0.8 0.8 0.8])
set(0, 'DefaultAxesColorOrder', [0 0 1]);

data=load('002.mat');
y=data.sig(:,2);
Fs = 30;
Ts = 1/Fs;
L=length(y); 


% bandpass filter
Wn=[0.5 8];
[b,a] = butter(2,Wn/(Fs/2),'bandpass');
y_filt=filtfilt(b,a,y);

% clipping signal
y_clip=y_filt;
for i=1:length(y_filt)
    if y_clip(i)<0
        y_clip(i)=0;
    end
end

% squared signal
y_square=y_clip.^2;

%first Moving Average
W1=3;
MA1 = 1/W1*ones(W1,1);
y_MA1 = filter(MA1,1,y_square);

%second moving average
W2=20;
MA2 = 1/W2*ones(W2,1);
y_MA2 = filter(MA2,1,y_square);

%define threshold
z=mean(y_square);
beta=0.02;
alpha=beta*z;

%threshold to generate block of interest
TH1=y_MA2+alpha;

%thrsehold to reject undesired block
%TH2=W2;

%generate blocks
for i=1:length(y_MA1)
    if y_MA1(i)>TH1(i)
    Blocks(i)=1;
    else
    Blocks(i)=0;
    end
end

%discard undesired blocks
%detect peak
y_peaks=y_filt;
for i=1:length(y_peaks)
if Blocks(i)==0
    y_peaks(i)=0;
end
end

%index for blocks window
t_index=0;
for i=1:length(Blocks)-1
   if Blocks(i)==0&&Blocks(i+1)==1 ||Blocks(i)==1&&Blocks(i+1)==0
      t_index=t_index+1;
      index(t_index)=i+1;
   end
end

%find maximum for each blocks
k=0;
for i=1:2:t_index-1
    k=k+1;
    [M,I]=max(y_peaks(index(i):index(i+1)));
    peak_index(k)=index(i)+I-1;
end

%[pks,locs] = findpeaks(y_peaks);
%y_peaks_detect=y_filt;
RR_interval=zeros(1,length(y_filt));
for k=1:length(peak_index)-1
    RR_interval(peak_index(k)+1:peak_index(k+1))=peak_index(k+1)-peak_index(k);
end

RR_interval(1:peak_index(1))=RR_interval(peak_index(1)+1);
RR_interval(peak_index(end):end)=RR_interval(peak_index(end));

RR_interval_time=RR_interval([1:Fs:length(RR_interval)-1]);
RR_interval_time_cut=RR_interval_time(1:end-7)';

RR_interval=RR_interval';

t=1:1:length(y_filt);
plot(t,y_filt,t(peak_index),y_filt(peak_index),'or')
hold on
plot(Blocks,'g')
figure
% display result
%plot(y_filt)
%hold on
plot(y_MA1,'r')
%hold on
%plot(y_MA2,'k')
hold on
plot(y_peaks,'g')

figure
plot(RR_interval)
figure
plot(RR_interval_time)
